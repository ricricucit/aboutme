# aboutme.it - HyperText Page Portfolio

### Descritpion / Motivation
This website has been created in 2018, as a simple portfolio for those, like me, who just need a simple page to show what they have been up to.

No blog, no anything. It's super minimal for fast loading and that's about it.

### Quick Start
1. **clone** this repo
2. run `npm i` to **install** dependencies
3. run `npm run dev` to **work** modify the project
4. run `npm run prod` to **build** for production
5. run `npm run deploy` to **build+deploy** with https://now.sh

### General Instructions
Check `package.json` to see possible commands for `development` and `production+deploy` and the `src/` folder which contains the incredibly simple and minimal code that creates the page.
